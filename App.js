import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import Search from './components/Search'
import Home from './components/Home'
import GetLocation from 'react-native-get-location'

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

 

const App= () => {
  
  GetLocation.getCurrentPosition({
    enableHighAccuracy: true,
    timeout: 15000,
})
.then(location => {
    console.log(location);
})
.catch(error => {
    const { code, message } = error;
    console.warn(code, message);
})

const Tab = createBottomTabNavigator()
  
  return (
    <>
      <StatusBar barStyle="dark-content" backgroundColor="#00AAFF" />
      <NavigationContainer>
        <Tab.Navigator
         screenOptions={({route})=>({
           tabBarIcon:({color})=>{
             let iconName;
             if(route.name==="home"){
               iconName = 'home-city-outline'
             }else if(route.name==="search"){
               iconName = "city"
             }
             return <View>
               <MaterialCommunityIcons name={iconName} size={25} color={color} />

             </View>
             
           }
         })}
         tabBarOptions={{
           activeTintColor:"white",
           inactiveTintColor:"gray",
           activeBackgroundColor:"#00AAFF",
           inactiveBackgroundColor:"#00AAFF"
         }}
        >
          <Tab.Screen name="home" component={Home} 
           initialParams={{city:"Barcelona"}}
          />
          <Tab.Screen name="search" component={Search} />
        </Tab.Navigator>
      </NavigationContainer>
   </>
  );
};
export default App;
